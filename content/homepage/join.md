---
title: "Rejoignez-nous"
weight: 3
header_menu: true
---

Vous souhaitez devenir Pastafarien ? Rien de plus simple : Il suffit de le vouloir. Pas besoin de remplir un dossier, suivre des cours ou payer quoi que ce soit. L'Église du MSV est ouverte à tout le monde, sans aucune distinction. Même les croyants d'autres religions sont les bienvenus ; s'ils le souhaitent, ils peuvent tester le Pastafarisme gratuitement pendant 30 jours. S'ils ne sont pas satisfaits, leur précédente religion les acceptera sans doute.

De plus, notre religion n'a qu'un seul dogme : c'est qu'il n'y a pas de dogme. Bien que le Monstre en Spaghettis Volant n'impose rien à ses fidèles, Il a malgré tout quelques recommandations et rites à nous proposer.


---

## Les huit « J'aimerais vraiment que tu ne »

10 à l'origine, 2 tablettes furent perdues. Les 8 "J'aimerais vraiment que tu ne" (ou 8 *condiments*) sont, en substance, les suivants :
```
1. J'aimerais vraiment que tu ne te comportes pas comme un abruti moralisateur, du genre « Plus saint que moi tu meurs » lorsque tu parleras de Ma Bonté Nouillesque.
2. J'aimerais vraiment que tu ne te serves pas de Mon Existence comme d'un prétexte pour opprimer, soumettre, punir, éviscérer...
3. J'aimerais vraiment que tu ne juges pas autrui sur son apparence, sur sa tenue vestimentaire ou sa façon de parler, ou, bon, écoute, sois sympa avec les autres, d'accord ?
4. J'aimerais vraiment que tu ne te livres pas à des activités qui iraient à ton encontre ou à celle de ta/ton partenaire consentant·e d'âge légal.
5. J'aimerais vraiment que tu ne t'attaques pas aux idées bigotes, misogynes et haineuses d'autrui le ventre vide.
6. J'aimerais vraiment que tu ne gaspilles pas des millions à construire des lieux saints en mon honneur, alors que cet argent serait mieux employé :
    - Dans la lutte contre la pauvreté
    - Dans la lutte contre les maladies
    - Pour vivre en paix, aimer avec passion, et faire baisser le prix d'internet
7. J'aimerais vraiment que tu ne passes pas ton temps à dire à tout le monde que je t'ai parlé.
8. J'aimerais vraiment que tu ne fasses pas à autrui ce que tu aimerais qu'on te fasses si tu es porté sur le BDSM
```
Les fidèles respectant au mieux ces recommendations seront probablement accueillis au Paradis, dont on sait qu'il comporte des volcans de bière et des usines à strip-teaseurs·euses, pour ceux que ça intéresse. L'Enfer existe également : il ressemble en tout points au Paradis sauf que la bière est éventée et les strip-teaseurs·euses ont des maladies vénériennes.

![Le MSV ouvrant les portes du Paradis](images/fsmheaven.jpg)

---

## Les fêtes

À différents moments de l'année, les croyants sont invités à se regrouper et célébrer le MSV.

##### Vendredi : Le jour saint

Une fois par semaine, les fidèles qui le souhaitent se regroupent pour partager un bon plat de pâtes et boire des bières (ou n'importe quelle autre boisson). Il convient de réciter une petite prière et de la ponctuer par un *Ramen*.

##### Halloween

Cette fête rend hommage aux peuple élu, les Pirates. Les fidèles sont inviter à se déguiser en Pirate et à distribuer des bonbons aux enfants.

##### Pâtes (ou Pastâques)

Durant ces vacances, les pastafariens sont encouragés à manger d'énormes quantités de pâtes et à participer au rituel du Passage du Bandeau Pirate, où chaque convive portera le bandeau sur l'oeil et exprimera sa joie d'avoir été touché·e par Son Appendice Nouillesque.

##### Ramendan

Cette fête se déroule à peu près à la même période que le Ramadan Musulman, mais à la différence du Ramadan, les pastafaristes ne prient ni ne jeûnent pas. Ils se contentent de passer quelque jours à ne consommer que des nouilles Ramen en souvenirs des étudiants désargentés.

##### La journée internationale « Parlez comme un Pirate »

Le 19 Septembre est l'occasion pour les pastafariens de célébrer leurs racines Pirates. Il est coutume de se vetir en Pirate et de parler à leur manière en buvant du Grog. C'est aussi l'occasion d'évangéliser les masses.

##### Les vacances de fin d'année

Cette célébration recouvre globalement l'ensemble des congés observés par les autres religions. 