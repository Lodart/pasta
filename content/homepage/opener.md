---
title: "Bienvenu·e"
weight: 1
---

L'Église du Monstre en Spaghettis Volant existe en secret depuis des milliers d'années, mais connut un renouveau il y a quelques années. 

Avec ses millions, voire milliers, de fervents adorateurs, l'Église du MSV est largement considérée comme une religion légitime, même par ses opposants - principalement des fondamentalistes Chrétiens, qui ont accepté que notre Dieu a de plus grosses boulettes que le leur.

Certains prétendent que le pastafarisme est purement une expérience de pensée ou de la satire, illustrant que le Dessein Intelligent n'est pas scientifique, mais juste une pseudoscience fabriquée par les Chrétiens pour imposer le Créationnisme dans les écoles. Ces personnes se trompent - L'Église du MSV est légitime et soutenue par la science. Tout ce qui ressemble à de l'humour ou de la satire n'est que pure coïncidence.