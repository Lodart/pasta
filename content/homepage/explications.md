---
title: "Le Pastafarisme Expliqué"
weight: 2
header_menu: true
---
Nous pensons que la religion - comme le Christianisme, l'Islam, le Pastafarisme - ne requière pas une croyance litérale pour provoquer l'illumination spirituelle. Une grande part de l'expérience transcendantale  de la religion peut être attribuée à la communauté. Et bien que certains membres soient de Vrais Croyants™©®, beaucoup ne le sont pas. Il y a de nombreux niveaux de croyance et aucun n'est plus ou moins légitime qu'un autre.

C'est à dire que vous n'avez pas à Croire pour faire partie de l'Église, mais nous espérons qu'en temps voulu vous verrez la Vérité. Mais les sceptiques, ainsi que les membres d'autres religions, sont toujours bienvenus.


---

## Génèse

Comme toutes les religions, l'Église du MSV tente d'expliquer la création du monde.

Il y a 5000 ans : le commencement. Le MSV créa la Terre et tout un tas d'autres planètes. Dans le but de tromper les scientifiques, il plaça chaque photon à des milliers d'années-lumière et enterra des os de dinosaures de manière à faire croire que la Terre est bien plus ancienne qu'elle n'est réellement.

![Représentation de la création](images/creation.jpg)

Cette supercherie continue encore aujourd'hui : Chaque fois qu'un scientifique met en place une expérience (mettons, une datation au carbone 14), Son appendice nouillesque intervient et change les résultats. On ne sait toutefois pas pourquoi il fait cela. Ses voies sont impénétrables.

---

## L'Ère des pirates

Le premier peuple à qui le MSV s'est révélé fut les pirates. En effet, il y a 2500 ans, ce peuple de joyeux explorateurs rencontra pour la première Son Appendice Nouillesque. Les pirates avaient coutume de distribuer des bonbons aux enfants, ce qui fut à l'origine d'Halloween. Loin d'être des voleurs et des hors-la-loi, ils étaient Son Peuple Élu, et suivaient Son Plan Divin, quel qu'il fût.

![Le MSV portant des vaisseaux pirates](images/fsmship.jpg)

Malheureusement, à partir de l'an 1700, les Hare Krishna, descendants des ninjas, déclarèrent la guerre aux pirates et en décimèrent l'immense majorité. Ce massacre est à l'origine du réchauffement climatique, comme le prouve ce graphique :

![Évolution de la température terrestre en fonction du nombre de pirates](images/piratechart.jpg)

---

## Aujourd'hui, la révélation

En 2005, le Monstre en Spaghettis se révéla au prophète Bobby Henderson. Ce dernier décida de contacter le conseil d'une école du Kansas pour réclamer qu'en parallèle de la théorie du Dessein Intelligent et de l'évolution, soit enseigné le Pastafarisme.

Vous pouvez lire le contenu de cette lettre [ici](https://www.spaghettimonster.org/about/open-letter/).

