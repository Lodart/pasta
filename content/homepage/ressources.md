---
title: "Ressources"
weight: 4
header_menu: true
---

L'Église du Monstre en Spaghettis Volant, comme les autres religions, possède différents textes sacrés. Un lecteur avisé pourra remarquer diverses exagérations, approximations voire mensonges. Tout cela est bien entendu intentionnel, dans le but d'éprouver notre foi et d'aiguiser notre sens critique.
![Le MSV Présentant la Bible](images/fsmbook.jpg)

##### L'Évangile

Le document le plus important pour l'Église du Monstre en Spaghettis Volants est sans doute *l'Évangile* :

 PDF : [En français](docs/Evangile_du_Monstre_en_Spaghettis_Volant.pdf) / [En anglais](docs/Gospel-Of-The-Flying-Spaghetti-Monster.pdf)
 
 Amazon : [En français](https://www.amazon.fr/%C3%89vangile-Monstre-Spaghettis-Volant-Henderson/dp/B093RTJHQ4) / [En anglais](https://www.amazon.com/Gospel-Flying-Spaghetti-Monster/dp/0812976568)

##### Le Canon Libre

 Par ailleurs, un recueil de textes pastafariens a également vu le jour (seulement en anglais) :

 *The Loose Canon* : [Site web](http://www.loose-canon.info/) / [PDF](docs/Loose-Canon-1st-Ed.pdf)

##### Le Petit Catéchisme du Monstre en Spaghettis Volant

*Le Petit Catéchisme du Monstre en Spaghettis Volant* est une bonne introduction francophone à l'Église du MSV de Victor Bonjean : 

[Amazon](https://www.amazon.fr/petit-Catechisme-Monstre-Spaghetti-Volant/dp/1543207995) / [EPUB](docs/Le_petit_Catechisme_du_Monstre_en_Spaghetti_Volant.epub)

##### L'Église Unitaire des Pâtes

 Enfin, le MSV s'est une nouvelle fois révélé, en 2018 à Violet Johnson. Il lui dicta le Nouveau Testament et Violet fonda par la suite L'Église Unitaire des Pâtes ([The Unitarian Church of Pasta](https://unitarianchurchofpasta.com/))

 *The New Testament of The Flying Spaghetti Monster; Dinner 2.0: The New and Improved Recipe!* : [PDF](docs/The_New_Testament_of_The_Flying_Spaghetti_Monster.pdf)

 Une nouvelle branche, se basant sur ce nouveau testament, vit aussi le jour en 2021 : [The FSM Revival Church of Ziti](https://ziti.works/app)
